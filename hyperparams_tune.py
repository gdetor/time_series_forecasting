# Hyperparameters tuning script based on Ray Framework.
# Copyright (C) 2020  Georgios Is. Detorakis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from torch.utils.data import DataLoader
from torch import nn, device
from torch.optim import Adam

from models.lstm import LSTM
from data_loader.timeseries_loader import TimeseriesLoader

from ray import tune


def train(config, checkpoint_dir=None):
    dev = device("cuda:0")

    noise_size = 1
    epochs = config['epochs']
    batch_size = config['batch_size']
    lrate = config['lrate']
    seq_len = config['sequence_len']
    n_layers = config['n_layers']
    hidden_dim = config['n_hidden']

    dropout = 0.2
    if n_layers == 1:
        dropout = 0.0

    name = '/home/gdetor/packages/time_series_forecasting/data/sinusoidal.npy'
    ts = TimeseriesLoader(data_path=name,
                          sequence_len=seq_len,
                          scale=True,
                          train=True,
                          horizon=1)
    data = DataLoader(ts,
                      batch_size=batch_size,
                      shuffle=False,
                      drop_last=True)

    net = LSTM(input_dim=noise_size,
               hidden_dim=hidden_dim,
               output_dim=noise_size,
               seq_len=seq_len,
               num_layers=n_layers,
               dropout=dropout,
               dev=dev).to(dev)

    optimizer = Adam(net.parameters(), lr=lrate)
    criterion = nn.MSELoss()

    for e in range(epochs):
        for x, y, s in data:
            x = x.to(dev)
            y = y.to(dev)

            optimizer.zero_grad()

            ypred = net(x)

            loss = criterion(ypred, y.view(-1, 1))
            loss.backward()
            optimizer.step()
        if e % 50 == 0:
            print("Epoch: %d  Loss: %f" % (e, loss.item()))
    returned_loss = loss.item() / batch_size
    tune.report(mean_loss=returned_loss)


if __name__ == '__main__':
    search_space = {"epochs": tune.choice([50, 100, 150, 200]),
                    "lrate": tune.uniform(1e-6, 1e-3),
                    "sequence_len": tune.choice([4, 8, 12, 24]),
                    "batch_size": tune.choice([8, 16, 32, 64]),
                    "n_layers": tune.choice([1, 2, 3, 4]),
                    "n_hidden": tune.choice([8, 16, 32, 64, 128, 256])}

    optimization = tune.run(train,
                            config=search_space,
                            num_samples=10,
                            metric="mean_loss",
                            mode="min",
                            raise_on_failed_trial=True,
                            local_dir="./tune_dir/",
                            resources_per_trial={"cpu": 3, "gpu": 1})
