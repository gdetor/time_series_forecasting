# This script contains the training function for the time series prediction
# neural networks in this repo.
# Copyright (C) 2020  Georgios Is. Detorakis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import argparse
from torch import nn, device, save
from torch.optim import Adam
from torch.utils.data import DataLoader

import numpy as np
import matplotlib.pylab as plt

from models.lstm import LSTM
from models.mlp import MLP
from models.tcn import TCN

from data_loader.timeseries_loader import TimeseriesLoader


dev = device("cuda:0")


def train(epochs,
          batch_size,
          lrate,
          n_layers,
          hidden_dim,
          seq_len,
          num_features,
          model_path='./results/lstm_forecast_model.pt',
          data_path='./data/sinusoidal.npy',
          model='lstm'):
    """
        Trains the neural networks for time series prediction (MLP, LSTM, TCN).

        Args:
            epochs (int)        Number of training epochs
            batch_size (int)    Batch size
            lrate (float)       Learning rate
            n_layers (int)      Number of hidden layers
            hidden_dim (int)    Number of units (neurons) in the hidden layers
            seq_len (int)       Sequence length for histortical (past) data
            num_features (int)  Dimension of features (1 for univariate time
            series)
            model_path (str)    Where to store the model (.pt file)
            data_path (str)     Where the training data are stored
            model (str)         Model type (see above)

        Returns: void
    """

    ts = TimeseriesLoader(data_path=data_path,
                          sequence_len=seq_len,
                          scale=True,
                          train=True,
                          horizon=1)
    data = DataLoader(ts,
                      batch_size=batch_size,
                      shuffle=False,
                      drop_last=True)

    if model == 'lstm':
        net = LSTM(input_dim=num_features,
                   hidden_dim=hidden_dim,
                   output_dim=num_features,
                   seq_len=seq_len,
                   num_layers=n_layers,
                   dropout=0.2,
                   dev=dev).to(dev)
    elif model == 'tcn':
        net = TCN(num_inputs=1,
                  num_channels=[32],
                  kernel_size=8,
                  seq_len=seq_len,
                  dropout=0.1).to(dev)
    else:
        lay_out = [seq_len*num_features]
        lay_out += [hidden_dim for i in range(n_layers)]
        lay_out += [1]
        norm = [True for i in range(1+n_layers)]
        norm += [False]
        net = MLP(lay_out=lay_out, normalize=norm).to(dev)

    optimizer = Adam(net.parameters(), lr=lrate)
    criterion = nn.MSELoss()

    loss_ = []
    for e in range(epochs):
        for x, y, s in data:
            if model == 'tcn':
                x = x.permute(0, 2, 1).to(dev)
            else:
                x = x.to(dev)
            y = y.to(dev)

            optimizer.zero_grad()
            ypred = net(x)

            loss = criterion(ypred, y.view(-1, 1))
            loss.backward()
            optimizer.step()
        loss_.append(loss.item() / batch_size)
        if e % 50 == 0:
            print("Epoch: %d  Loss: %f" % (e, loss.item()))
    save(net, model_path)
    loss_ = np.array(loss_)
    np.save("./results/loss", loss_)
    plt.figure()
    plt.plot(loss_)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LSTM Forecasting Test')
    parser.add_argument('--epochs', type=int, default=300, metavar='N',
                        help='number of epochs for training (default: 300)')
    parser.add_argument('--batch-size', type=int, default=128, metavar='N',
                        help='input batch size for training (default: 128)')
    parser.add_argument('--learning-rate', type=float, default=2e-3,
                        metavar='N',
                        help='learning rate for training (default: 2e-3)')
    parser.add_argument('--num-layers', type=int, default=2, metavar='N',
                        help='number of layers (default: 2)')
    parser.add_argument('--hidden-dim', type=int, default=128, metavar='N',
                        help='dimension of hidden layers (default: 128)')
    parser.add_argument('--seq-len', type=int, default=20, metavar='N',
                        help='input sequence length (default: 20)')
    parser.add_argument('--num-features', type=int, default=1, metavar='N',
                        help='input dimension (default: 1)')
    parser.add_argument('--data-path', type=str,
                        default='./data/sinusoidal.npy',
                        metavar='N',
                        help='Data set full path')
    parser.add_argument('--model-path', type=str,
                        default='./results/lstm.pt',
                        metavar='N',
                        help='Trained model full path')
    parser.add_argument('--model', type=str,
                        default='lstm',
                        metavar='N',
                        help='model type')
    args = parser.parse_args()
    train(args.epochs,
          args.batch_size,
          args.learning_rate,
          args.num_layers,
          args.hidden_dim,
          args.seq_len,
          args.num_features,
          args.model_path,
          args.data_path, args.model)
