# Time series forecasting with (deep) neural networks

This repository contains basic models for performing time series
prediction (forecasting). Three models are implemented in Pytorch:
an MLP (feed-forward), an LSTM, and a TCN (Temporal CNN). You
will find a time series data loader along with the neural networks.


### Repo contents

The directory `models/` contains the Pytorch implementations of
an MLP, an LSTM, and a TCN. In the directory `data_loader/`, you
will find a Pytorch class for loading and pre-processing time
series data sets (see [here](https://github.com/gdetor/pytorch_timeseries_loader)
for more details). The directory `data/` contains a sample data
set file (`.npy`) with a sinusoidal signal.


### How to run

Before running the scripts, you should create a directory named
`results/` in the parent directory of your cloned repo and make
sure you have all the data sets available. A sinusoidal signal
is provided as data set sample in the directory `data/`. Then
you can execute the following commands for training/testing.

```bash
$ python3 train.py --epochs X --batch-size X --learning-rate X --num-layers X --hidden-dim X --seq-len X --num-features X --data-path ./data/sinusoidal.npy --model-path ./results/mlp.pt --model mlp
```

You can choose one of the three neural networks by setting the
flag --model to 'mlp', 'lstm', or 'tcn'. 
Furthermore, you will have to set all the necessary parameters,
such as the number of epochs, hidden layers, hidden units, learning
rate, and batch size. In addition, you can determine the number
of features (this number must be one for univariate time series)
and the number of historical (past) data points that the neural
network will use to predict future points (sequence_lenght). 

The script `test.py` will run a basic validation test on the
trained model. You can run it passing as arguments the batch
size (use 1), the sequence length (this must be the same number
you used during the training of your model), the full path
where your data file is, the full path of your trained model,
and finally the type of your model ('mlp', 'lstm', 'tcn'). 

```bash
$ python3 test.py --batch-size 1 --seq-len X --data-path ./data/sinusoidal.npy --model-path ./results/mlp.pt --model mlp
```

Moreover, you can optimize the hyperparameters for the LSTM model
by running the script `hyperparams_tune.py`. Before running the script,
make sure you have created a directory names `tune_dir/` (Ray will
save in here the log files).

```bash
$ python3 hyperparams_tune.py
```
You can adjust the parameters optimization script to optimize any of the three
provided networks. 


> The present repository provides only a few implementations of neural
   networks for time series prediction. Its primary purpose is to
   demonstrate how one can use neural networks (and deep neural networks)
   in time series predictions.


### Dependencies

Scripts in this repo have the following dependencies:
  - Numpy
  - Matplotlib
  - Sklearn
  - Torch (Pytorch)
  - Ray (1.8.0)
