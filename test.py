# Test function for trained neural networks for time series prediction.
# Copyright (C) 2020  Georgios Is. Detorakis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import argparse
import numpy as np
import matplotlib.pylab as plt

from torch import load, no_grad, device
from torch.utils.data import DataLoader

from data_loader.timeseries_loader import TimeseriesLoader
# from models.lstm import LSTM


dev = device("cpu")


def test(seq_len=20,
         batch_size=1,
         model='lstm',
         model_path='./results/lstm.pt',
         data_path='./data/sinusoidal.npy'):
    """
        Tests a trained neural network on time series prediction. The trained
        model should be located in a local directory (e.g., ./results/).
        The function will plot some of the results.
         Args:
             seq_len (int)       Sequence length for histortical (past) data
             batch_size (int)    Batch size
             model (str)         The type of the model (mlp, lstm, tcn)
             model_path (str)    Where to find the model (.pt file)
             data_path (str)     Where the training data are located

         Returns: void
    """

    ts = TimeseriesLoader(data_path=data_path,
                          sequence_len=seq_len,
                          scale=True,
                          train=False,
                          horizon=1)
    data = DataLoader(ts,
                      batch_size=batch_size,
                      shuffle=False,
                      drop_last=True)

    net = load(model_path)
    net = net.to(dev)
    net.flag = 0
    print(net)

    net.eval()
    d = iter(data)
    y_pred, y_target = [], []
    with no_grad():
        for _ in range(30):
            x, y, _ = next(d)
            if model == 'tcn':
                x = x.permute(0, 2, 1).to(dev)
            else:
                x = x.to(dev)
            y_target.append(y.detach().cpu().numpy())
            y_pred.append(net(x).cpu().numpy())
    y_pred = np.array(y_pred)
    y_target = np.array(y_target)
    np.save(model_path, y_pred)
    plt.plot(y_pred[:, 0, 0], 'k-x', lw=2, ms=10, label='Predictions')
    plt.plot(y_target[:, 0, 0, 0], 'r-o', lw=2, ms=10, label='Targets')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LSTM Forecasting Test')
    parser.add_argument('--batch-size', type=int, default=1, metavar='N',
                        help='input batch size for training (default: 1)')
    parser.add_argument('--seq-len', type=int, default=20, metavar='N',
                        help='input sequence length (default: 20)')
    parser.add_argument('--model', type=str, default='lstm', metavar='N',
                        help='Model type (default: lstm)')
    parser.add_argument('--data-path', type=str,
                        default='./data/sinusoidal.npy',
                        metavar='N',
                        help='Data set full path')
    parser.add_argument('--model-path', type=str,
                        default='./results/lstm.pt',
                        metavar='N',
                        help='Trained model full path')

    args = parser.parse_args()
    test(seq_len=args.seq_len,
         batch_size=args.batch_size,
         model=args.model,
         model_path=args.model_path,
         data_path=args.data_path)
