# LSTM implementation for time series prediction.
# Copyright (C) 2020  Georgios Is. Detorakis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from torch import device, randn
from torch import nn


class Identity(nn.Module):
    """
        Identity class, implements an identity layer.
    """
    def __init__(self):
        """
        Constructor of Identity class.

        Args:
            None

        Returns:
        """
        super(Identity, self).__init__()

    def forward(self, x):
        """
        Forward method of Identity class.

        Args:
            x (torch tensor):   Input tensor

        Returns:
            A torch tensor identical to input.
        """
        return x


class LSTM(nn.Module):
    """
    LSTM class used for time series forecasting.
    """
    def __init__(self,
                 input_dim=1,
                 hidden_dim=1,
                 output_dim=1,
                 seq_len=1,
                 num_layers=1,
                 dropout=0.2,
                 dev=device("cpu")):
        """
        Constructor of LSTM class.

        Args:
            input_dim (int):    Number of features (input dimension)
            hidden_dim (int):   Number of units per hidden layer
            output_dim (int):   Output dimension (number of features)
            seq_len (int):      Input sequence length
            num_layers (int):   Number of layers
            dropout (float):    Dropout probability ([0, 1])

        Returns:

        """
        super(LSTM, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        self.output_dim = output_dim
        self.dev = dev

        if self.num_layers == 1:
            dropout = 0.0

        # Initialize an Pytorch LSTM object
        self.lstm = nn.LSTM(self.input_dim,
                            self.hidden_dim,
                            self.num_layers,
                            dropout=dropout,
                            bidirectional=False,
                            batch_first=True)
        # Initialize a Linear layer (output)
        self.fc1 = nn.Linear(self.hidden_dim * seq_len, self.output_dim)

        # Set the flag for the initial hidden and cell states
        # flag is set to 1 after the first run to retain the states
        self.flag = 0

        # Xavier initialization of LSTM weights
        for layer in self.lstm._all_weights:
            for p in layer:
                if 'weight' in p:
                    nn.init.xavier_uniform_(self.lstm.__getattr__(p).data)

        # Bias inititialization for the LSTM
        for names in self.lstm._all_weights:
            for name in filter(lambda n: "bias" in n,  names):
                bias = self.lstm.__getattr__(name)
                n = bias.size(0)
                start, end = n//4, n//2
                bias.data[start:end].fill_(1.)

    def forward(self, x):
        """
        Forward method of LSTM class.

        Args:
            x (torch tensor):   Input tensor
                                (batch_size, sequence_len, num_features)

        Returns:
            A pytorch tensor (prediction)
        """
        batch_size = x.shape[0]
        dev = x.device
        # Initialize the hidden and cell states and keep them intact during
        # training
        if self.flag == 0:
            self.h0 = nn.Parameter(randn(self.num_layers*1, batch_size,
                                         self.hidden_dim),
                                   requires_grad=True).to(dev)
            self.c0 = nn.Parameter(randn(self.num_layers*1, batch_size,
                                         self.hidden_dim),
                                   requires_grad=True).to(dev)
            self.flag = 1
        out, lstm_hidden = self.lstm(x, (self.h0, self.c0))
        # In case you do a whole sequence forecasting uncomment the line bellow
        # out = self.fc1(out.permute(1, 0, 2)).permute(1, 0, 2)
        out = self.fc1(out.reshape(-1, out.shape[1] * out.shape[2]))
        return out
