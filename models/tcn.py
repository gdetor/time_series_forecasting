# Temporal CNN implementation for time series prediction.
# Copyright (C) 2020  Georgios Is. Detorakis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import torch.nn as nn
from torch.nn.utils import weight_norm


class Chunk1d(nn.Module):
    """
    Chunk1d class. This class chunks the input tensor based on a given chunk
    size.
    """
    def __init__(self, chunk_size):
        super(Chunk1d, self).__init__()
        self.chunk_size = chunk_size

    def forward(self, x):
        return x[:, :, :-self.chunk_size].contiguous()


class TCNResidualBlock(nn.Module):
    """
        Temporal Convolutionanl Residual Block Class.
    """
    def __init__(self, n_inputs, n_outputs, kernel_size, stride, dilation,
                 padding, dropout=0.2):
        """
        Constructor of TCNResidualBlock class.

        Args:
            n_inputs (int):     Number of input channels (input dimension)
            n_outputs (int):    Number of output channels
            kernel_size (int):  Kernel size
            stire (int):        Convolution stride size
            dilation (int):     Convolution dilation size
            padding (int):      Convolution padding size
            dropout (float):    Dropout probability [0, 1]

        Returns:

        """
        super(TCNResidualBlock, self).__init__()
        self.conv1 = weight_norm(nn.Conv1d(n_inputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding,
                                           dilation=dilation))
        self.chunk2 = Chunk1d(padding)
        self.relu1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)

        self.conv2 = weight_norm(nn.Conv1d(n_outputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding,
                                           dilation=dilation))
        self.chunk1 = Chunk1d(padding)
        self.relu2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)

        self.net = nn.Sequential(self.conv1, self.chunk1, self.relu1,
                                 self.dropout1,
                                 self.conv2, self.chunk2, self.relu2,
                                 self.dropout2)
        if n_inputs != n_outputs:
            self.downsample = nn.Conv1d(n_inputs, n_outputs, 1)
        else:
            self.downsample = None
        self.relu = nn.ReLU()
        self.init_weights()

    def init_weights(self):
        """
        Initialize convolutional layer weights from Normal distribution
        """
        self.conv1.weight.data.normal_(0, 0.01)
        self.conv2.weight.data.normal_(0, 0.01)
        if self.downsample is not None:
            self.downsample.weight.data.normal_(0, 0.01)

    def forward(self, x):
        """
        Forward method of TCNResidualBlock class.

        Args:
            x (torch tensor):   Input tensor (batch_size, channels, seq_len)

        Returns:
            Torch tensor
        """
        out = self.net(x)
        res = x if self.downsample is None else self.downsample(x)
        return self.relu(out + res)


class TCN(nn.Module):
    """
    TCN class. Implements a simple TCN network with one FC output layer used
    for time-series prediction.
    """
    def __init__(self, num_inputs, num_channels, seq_len=1, kernel_size=2,
                 dropout=0.2):
        super(TCN, self).__init__()
        """
        Constructor of TCN class.

        Args:
            num_inputs (int):    Input dimension (number of features)
            num_channels (list int):  Number of channels
            seq_len (int):      Sequence length
            kernel_size (int):  Kernel size
            dropout (float):    Dropout probability [0, 1]

        Returns:

        """
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i
            in_channels = num_inputs if i == 0 else num_channels[i-1]
            out_channels = num_channels[i]
            layers += [TCNResidualBlock(in_channels, out_channels, kernel_size,
                                        stride=1, dilation=dilation_size,
                                        padding=(kernel_size-1)*dilation_size,
                                        dropout=dropout)]
        self.fc = nn.Linear(num_channels[-1] * seq_len, 1)
        self.tanh = nn.Tanh()
        self.network = nn.Sequential(*layers)

    def forward(self, x):
        """
        Forward method of TCN class.

        Args:
            x (torch tensor):   Input tensor (batch_size, channels, seq_len)

        Returns:
            Torch tensor:       Prediction (batch_size, 1)
        """
        out = self.network(x)
        out = out.view(-1, out.shape[1] * out.shape[2])
        out = self.tanh(self.fc(out))
        return out
