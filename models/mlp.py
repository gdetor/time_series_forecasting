# MLP implementation for time series prediction
# Copyright (C) 2020  Georgios Is. Detorakis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from torch import nn


class MLP(nn.Module):
    """
    Multi-Layer Perceptron class for time series forecasting.
    """
    def __init__(self, lay_out=(10, 10, 1), normalize=(True, False)):
        """
        Constructor of MLP class.

        Args:
            lay_out (ints tuple):   Configuration of the MLP
            normalize (bool tuple): Enables/disables normalization per layer

        Returns:

        """
        super(MLP, self).__init__()

        m = len(lay_out)
        self.model = nn.Sequential()
        for i in range(m-1):
            in_feat = lay_out[i]
            out_feat = lay_out[i+1]
            self.model.add_module('fc'+str(i), nn.Linear(in_feat, out_feat))
            if normalize[i]:
                self.model.add_module('batchnorm'+str(i),
                                      nn.BatchNorm1d(out_feat))
            self.model.add_module('relu', nn.ReLU())
        self.model.add_module('sigmoid', nn.Sigmoid())

    def forward(self, x):
        """
        Forward method of MLP class.

        Args:
            x (torch tensor):   The input sequence (sequence length x number
                                                    of features)

        Returns:
            A pytorch tensor that contains the prediction.
        """
        x = x.view(-1, x.shape[1] * x.shape[2])
        out = self.model(x)
        return out
